<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class MemberController extends Controller
{
    public function index()
    {
    	return view('member_view');
    }

    public function AddLogo()
    {
    	return view('add_logo');
    }

    public function InsertLogo(Request $request)
    {
    	$image = $request->file('logo_image');
    	 if ($image) {
            $image_name= str_random(5);
            $ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $upload_path='public/Logo/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            if ($success) {
                $data['logo_image']=$image_url;
                $employee=DB::table('logo')
                         ->insert($data);
              if ($employee) {
                 $notification=array(
                 'messege'=>'Successfully Logo Inserted ',
                 'alert-type'=>'success'
                  );
                return Redirect()->back()->with($notification);                      
             }else{
              $notification=array(
                 'messege'=>'error ',
                 'alert-type'=>'success'
                  );
                 return Redirect()->back()->with($notification);
             }      
                
            }else{

              return Redirect()->back();
            	
            }
        }else{
        	  return Redirect()->back();
        }

    }

    public function AddMember()
    {
    	return view('add_member');
    }

    public function InsertMember(Request $request)
    {
    	$data = array();
    	$data['name'] = $request->name;
    	$data['email'] = $request->email;
    	$data['member_id'] = $request->member_id;
    	$data['phone_number'] = $request->phone_number;
    	$insert_member = DB::table('member')->insert($data);
        if ($insert_member) {
                 $notification=array(
                 'messege'=>'Successfully Member Inserted ',
                 'alert-type'=>'success'
                  );
                return Redirect()->back()->with($notification);                      
             }else{
              $notification=array(
                 'messege'=>'error ',
                 'alert-type'=>'success'
                  );
                 return Redirect()->back()->with($notification);
             } 
    } 

    public function AllMember()
    {
    	$all_member = DB::table('member')->get();
    	return view('all_member', compact('all_member'));
    }

    public function AddContributor()
    {
        return view('add_cont');
    }

    public function InsertCont(Request $request)
    {
        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['cont_id'] = $request->cont_id;
        $data['phone'] = $request->phone;
        $insert_data = DB::table('cont')->insert($data);
        if ($insert_data) {
                 $notification=array(
                 'messege'=>'Successfully Contributor Inserted ',
                 'alert-type'=>'success'
                  );
                return Redirect()->back()->with($notification);                      
             }else{
              $notification=array(
                 'messege'=>'error ',
                 'alert-type'=>'success'
                  );
                 return Redirect()->back()->with($notification);
             } 
    }

    public function AllContributor()
    {
        $all_cont = DB::table('cont')->get();
        return view('all_cont', compact('all_cont'));
    }


    public function EditCont($id)
    {
        $edit_cont = DB::table('cont')->where('id', $id)->first();
        return view('edit_cont', compact('edit_cont'));
    }

    public function UpdateCont(Request $request, $id)
    {
        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['cont_id'] = $request->cont_id;
        $data['phone'] = $request->phone;
        $update_data = DB::table('cont')->where('id', $id)->update($data);
         if ($update_data) {
                 $notification=array(
                 'messege'=>'Successfully Contributor Updated ',
                 'alert-type'=>'success'
                  );
                return Redirect()->back()->with($notification);                      
             }else{
              $notification=array(
                 'messege'=>'error ',
                 'alert-type'=>'success'
                  );
                 return Redirect()->back()->with($notification);
             } 
    }

    public function ViewCont($id)
    {
        $view_cont = DB::table('cont')->where('id', $id)->first();
        return view('view_cont', compact('view_cont'));
    }

    public function DeleteCont($id)
    {
        $delete_cont = DB::table('cont')->where('id', $id)->delete();
        if ($delete_cont) {
                 $notification=array(
                 'messege'=>'Successfully Contributor Deleted ',
                 'alert-type'=>'success'
                  );
                return Redirect()->back()->with($notification);                      
             }else{
              $notification=array(
                 'messege'=>'error ',
                 'alert-type'=>'success'
                  );
                 return Redirect()->back()->with($notification);
             }
    }

    public function EditMember($id)
    {
        $edit_member = DB::table('cont')->where('id', $id)->first();
        return view('edit_member', compact('edit_member'));
    }
}
