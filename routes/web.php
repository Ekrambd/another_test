<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/member', 'MemberController@index');
Route::get('/add-logo', 'MemberController@AddLogo');
Route::post('/insert-logo', 'MemberController@InsertLogo');

//Member
Route::get('/add-member', 'MemberController@AddMember');
Route::post('/insert-member', 'MemberController@InsertMember');
Route::get('/all-member', 'MemberController@AllMember');
Route::get('/edit-member/{id}', 'MemberController@EditMember');


//cont

Route::get('/add-contributor', 'MemberController@AddContributor');
Route::post('/insert-cont', 'MemberController@InsertCont');
Route::get('/all-contributor', 'MemberController@AllContributor');
Route::get('/edit-cont/{id}', 'MemberController@EditCont');
Route::post('/update-cont/{id}', 'MemberController@UpdateCont');
Route::get('/view-cont/{id}', 'MemberController@ViewCont');
Route::get('/delete-cont/{id}', 'MemberController@DeleteCont');