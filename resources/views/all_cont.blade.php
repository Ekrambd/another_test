@extends('layouts.app')
@section('content')
<div class="content-page">
  <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Welcome !</h4>
                    <ol class="breadcrumb pull-right">
                        <li><a href="#">Echobvel</a></li>
                        <li class="active">IT</li>
                    </ol>
                </div>
            </div>

            <!-- Start Widget -->
            <div class="row">
	          <div class="col-md-12">
	              <div class="panel panel-default">
	                  <div class="panel-heading">
	                      <h3 class="panel-title">All Contributor </h3>
	                      <a href="{{ URL::to('/add-contributor') }}" class="btn btn-sm btn-info pull-right">Add New</a>
	                  </div>
	                  <div class="panel-body">
	                      <div class="row">
	                          <div class="col-md-12 col-sm-12 col-xs-12">
	                              <table id="datatable" class="table table-striped table-bordered">
	                                  <thead>
	                                      <tr>
	                                          <th>Contributor ID</th>
	                                          <th>Name</th>
	                                          <th>Email</th>
	                                          <th>Phone</th>

	                                          <th>Action</th>
	                                      </tr>
	                                  </thead>

	                           
	                                  <tbody>
	                                  	@foreach($all_cont as $row)
	                                      <tr>
	                                          <td>{{ $row->id }}</td>
	                                          <td>{{ $row->name }}</td>
	                                          @if($row->email == NULL)
	                                           <td>No Email Yet</td>
	                                          @else
	                                            <td>{{$row->email}}</td>
	                                          @endif
	                                          <td>{{$row->phone}}</td>
	                                         <td>
	                                         	<a href="{{ URL::to('/edit-cont/'.$row->id) }}" class="btn btn-sm btn-info">Edit</a>
	                                         	<a href="{{ URL::to('/delete-cont/'.$row->id) }}" class="btn btn-sm btn-danger" id="delete">Delete</a>
	                                         	<a href="{{ URL::to('/view-cont/'.$row->id) }}" class="btn btn-sm btn-success" >View</a>

	                                         </td>
	                                      </tr>
	                                    @endforeach
	                                  </tbody>
	                              </table>
	                          </div>
	                      </div>
	                  </div>
	              </div>
	          </div>
            </div>
        </div> <!-- container -->            
    </div> <!-- content -->
</div>


@endsection